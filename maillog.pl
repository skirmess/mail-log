#!/usr/bin/perl

use warnings;
use strict;

use File::Glob ':globally';
use Digest::MD5 qw(md5_base64);
use File::Basename;
use Carp;

use Getopt::Long;

my $state_file;
my $html      = 0;
my $recipient = 'root';
my $subject   = 'log file';
my @ignore;

my $sendmail  = undef;
my $max_lines = 200;
my $current_line;

main();
exit 0;

sub main {
    parse_options();

    my $last_mailed_log_line = process_log_files(@ARGV);

    return if ( !defined $last_mailed_log_line );

    return write_last_reported_md5_to_state_file(
        md5_base64($last_mailed_log_line), $state_file );
}

sub usage {
    my $name       = basename $0;
    my $name_space = q{ } x length $name;
    print {*STDERR}
        "usage: $name [ --recipient recipient ] [ --subject subject ]\n";
    print {*STDERR}
        "       $name_space [ --html ] [ --ignore <pattern> ] ...\n";
    print {*STDERR} "       $name_space --state state_file <log_file> ...\n";
    exit 255;
}

sub parse_options {
    GetOptions(
        "html"        => \$html,
        "ignore=s"    => \@ignore,
        "recipient=s" => \$recipient,
        "state=s"     => \$state_file,
        "subject=s"   => \$subject,
    ) or usage();

    if ( !defined $state_file ) {
        usage();
    }

    if ( scalar @ARGV == 0 ) {
        usage();
    }

    for my $ignore (@ignore) {
        $ignore = qr/$ignore/;
    }

    return;
}

sub get_last_reported_md5_from_state_file {
    my ($state_file) = @_;

    my $state = 'x';

    if ( !-f $state_file ) {
        return $state;
    }

    open my $fh, '<', $state_file
        or croak "$!";

    my $last_reported_md5_raw = <$fh>;
    if ( defined $last_reported_md5_raw ) {
        chomp $last_reported_md5_raw;
        $state = $last_reported_md5_raw;
    }

    close $fh
        or croak "$!";

    return $state;
}

sub mail_line {
    my ($log_line) = @_;

    if ( !defined $log_line ) {
        close_sendmail();
    }

    if ( !defined $sendmail ) {
        open_sendmail();
    }

    print $sendmail $log_line, "\n";
    $current_line++;

    if ( $current_line >= $max_lines ) {
        close_sendmail();
    }

    return;
}

sub open_sendmail {
    my @sendmail_cmd = ( 'sendmail', '-i', '-t' );

    open $sendmail, '|-', @sendmail_cmd
        or croak "$!";

    print $sendmail "To: $recipient\n";
    print $sendmail "Subject: $subject\n";

    if ($html) {
        print $sendmail "MIME-Version: 1.0\n";
        print $sendmail "Content-Type: text/html\n";
        print $sendmail "Content-Disposition: inline\n";
        print $sendmail "\n";
        print $sendmail "<html>\n";
        print $sendmail "<body>\n";
        print $sendmail
            '<pre style="font-family: Consolas, Courier New, Courier, monospace; font-size: 14px">'
            . "\n";
    }

    print $sendmail "\n";

    $current_line = 0;

    return;
}

sub close_sendmail {
    return if ( !defined $sendmail );

    if ($html) {
        print $sendmail "</pre>\n";
        print $sendmail "</body>\n";
        print $sendmail "</html>\n";
    }

    close $sendmail
        or croak "$!";

    $sendmail = undef;

    # sleep 2 seconds to sort the mails in the correct order in the inbox
    sleep 2;

    return;
}

sub write_last_reported_md5_to_state_file {
    my ( $md5, $state_file ) = @_;

    open my $fh, '>', $state_file
        or croak "$!";

    print $fh $md5;

    close $fh
        or croak "$!";

    return;
}

sub process_log_files {
    my @files = @_;
    my $new_last_md5;
    my @new_log_lines;

    my $last_reported_md5
        = get_last_reported_md5_from_state_file($state_file);

    my $file = shift @files;

    open my $fh, '<', $file
        or croak "Cannot read $file: $!";

    my $found = 0;

LINE:
    while ( my $log_line = <$fh> ) {
        chomp $log_line;

        my $md5 = md5_base64($log_line);

        if ( $md5 eq $last_reported_md5 ) {
            $found = 1;
            last LINE;
        }
    }

    my $last_mailed_log_line = undef;

    if ( !$found ) {
        if ( scalar @files > 0 ) {
            $last_mailed_log_line = process_log_files(@files);
        }
        seek $fh, 0, 0 or croak "Cannot seek $file: $!";
    }

LOG_LINE:
    while ( my $log_line = <$fh> ) {
        chomp $log_line;

        for my $ignore (@ignore) {
            next LOG_LINE
                if ( $log_line =~ $ignore );
        }

        mail_line($log_line);
        $last_mailed_log_line = $log_line;
    }

    close $fh
        or croak "Cannot read $file: $!";

    return $last_mailed_log_line;
}
