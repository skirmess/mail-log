maillog
========

Send all new entries from a log file per email

usage: maillog.pl [ --recipient recipient ] [ --subject subject ]
                  --state state_file <log_file> ...

 --recipient recipient
		The mail report is sent with sendmail. This is the address
		passed through to sendmail. Defaults to "root". Optional.

 --subject subject
		The subject of the email. This is passed to sendmail.
		Defaults to "log file". Optional.

 --state state_file
		The file in which maillog stores the md5 sum of the last sent
		line from the log. Required.

 --html
		Create an HTML mail. Defaults to text mail.

 --ignore pattern
		Lines that match this pattern are ignored. Can be specified
		multiple times.

 log_file
		The log file to scan for new entries and optional rotated log
		files.

Examples:

	Send all new entries from the file /var/adm/messages to user root with
	subject "log file". The state is saved in file /root/messages.state.
	If maillog cannot find the last line it sent in /var/adm/messages it
	scans /var/adm/messages.0 (and so on). That means it will even report
	new entries from a rotated log file /var/adm/messages.0.

	maillog.pl --state /root/messages.state /var/adm/messages*

